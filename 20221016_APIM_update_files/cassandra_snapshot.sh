#!/bin/bash

KEYSPACE_NAME=x2035b5c4_50ec_4502_9ceb_85cc53647fcd_group_3


/opt/axway/cassandra/bin/nodetool -h localhost snapshot -t "APIM_Cassandra_$(date +%Y%m%d)" $KEYSPACE_NAME
/opt/axway/crontab_script/Cassandra_snapshot_backup_script.sh

#刪除30天前資料
find /opt/axway/dbbackup/Cassandra/* -type d -mtime +30 -exec rm -rf {} \;